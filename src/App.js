import './App.css';
import { ChildButton, SuperButton } from './components/ExtendButton';
import { PropButton, PrimaryButton } from './components/PropButton';
import StyledButton from './components/StyledButton';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {

  return (
    <div>
      <div>
        <StyledButton>I am a Style Button !</StyledButton>
      </div>
      <div>
        <PropButton >Button 1</PropButton>
        <PropButton bgColor={"#FFDACA"} fontColor={"#D85E18"}>Button 2</PropButton>
        <PropButton bgColor={"#FFF4C7"} fontColor={"#BB9923"}>Button 3</PropButton>
      </div>
      <div>
        <PrimaryButton primary>Primary Button</PrimaryButton>
        <PrimaryButton>Orther Button 1</PrimaryButton>
        <PrimaryButton>Orther Button 2</PrimaryButton>
      </div>
      <div>
        <SuperButton>Super Button</SuperButton>
        <ChildButton>Hello 1</ChildButton>
        <ChildButton bgColor={"#FF69B4"} fontColor={"green"}>Hello 1</ChildButton>
        <ChildButton bgColor={"yellow"} fontColor={"green"}>Hello 1</ChildButton>
      </div>
    </div>
  );
}

export default App;

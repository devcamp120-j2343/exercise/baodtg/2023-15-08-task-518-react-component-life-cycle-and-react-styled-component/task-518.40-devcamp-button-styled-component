import { styled } from "styled-components";


const PropButton = styled.button`
    background-color: ${props => props.bgColor || "red"};
    color: ${props => props.fontColor || "white"};
    border: none;
    border-radius: 5px;
    font-size: 18px;
    font-weight: 500;
    padding: 10px;
    margin: 10px;
`
const PrimaryButton = styled.button`
    background-color: ${props => props.primary ? "#7B4CD8" : "#FF31CA"};
    color: ${props => props.primary ? "white" : "salmon"};
    border: none;
    border-radius: 5px;
    font-size: 18px;
    font-weight: 500;
    padding: 10px;
    margin: 10px;

// `
// const PrimaryButton = styled.button`
// ${props => props.primary ?
//         `background-color: #7B4CD8; color: white;` :
//         `background-color: #FF31CA; color: salmon;`
//     }
//     border: none;
//     border-radius: 5px;
//     font-size: 18px;
//     font-weight: 500;
//     padding: 10px;
//     margin: 10px;

// `
export { PropButton, PrimaryButton } 